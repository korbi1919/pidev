using System.Web.Mvc;
using Microsoft.Practices.Unity;
using Unity.Mvc4;
using PIdevService;
using PIdevData.Infrastructure;
using PIdevData.Repositories;

namespace PImvc
{
  public static class Bootstrapper
  {
    public static IUnityContainer Initialise()
    {
      var container = BuildUnityContainer();

      DependencyResolver.SetResolver(new UnityDependencyResolver(container));

      return container;
    }

    private static IUnityContainer BuildUnityContainer()
    {
        var container = new UnityContainer(); 
        container.RegisterType<IClientService, ClientService>();

       
    
        container.RegisterType<IUnitOfWork, UnitOfWork>();
        container.RegisterType<IClientRepository, ClientRepository>();
        container.RegisterType<IVeterinaryRepository, VeterinaryRepository>();

   
        container.RegisterType<IDatabaseFactory, DatabaseFactory>();
        return container;
    }

    public static void RegisterTypes(IUnityContainer container)
    {
    
    }
  }
}