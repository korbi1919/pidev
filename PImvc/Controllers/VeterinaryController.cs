﻿using PIdevDomain.Entities;
using PIdevService;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace PImvc.Controllers
{
    public class VeterinaryController : Controller
    {

        private readonly IClientService clientService;

        public VeterinaryController(IClientService clientService)
        {

            this.clientService = clientService;

        }
        //
        // GET: /Client/
        public ActionResult Index()
        {

            var veterinaries = clientService.GetVeterinaries();

            return View(veterinaries);
        }

        [HttpGet]
        public ActionResult Edit(int id)
        {

            var c = clientService.GetClient(id);
            return View(c);
        }



        [HttpPost]
        public ActionResult Edit(Veterinary veterinary)
        {


            if (ModelState.IsValid)
            {
                clientService.UpdateVeterinaryDetached(veterinary);
                clientService.SaveClient();
                return RedirectToAction("Index");
            }
            return View(veterinary);

        }

        [HttpGet]
        public ActionResult Create()
        {

            var veterinary = new Veterinary();
            return View(veterinary);
        }
        //
        // POST: /Client/Create
        [HttpPost]
        public ActionResult Create(Veterinary f)
        {
            if (!ModelState.IsValid)
            {
                return View(f);
            }
            clientService.CreateVeterinary(f);
            clientService.SaveClient();
            return RedirectToAction("Index");
        }

        

        [HttpGet]
        public ActionResult Delete(int id)
        {

            //var c = clientService.GetClient(id);
            //return View(c);
            clientService.DeleteClientV(id);
            clientService.SaveClient();
            //var farmers = clientService.GetFarmers();
            return RedirectToAction("Index");
        }

       

        [HttpGet]
        public ActionResult listRegion(String region)
        {

            var c = clientService.GetVetReg(region);
            return View(c);
        }

       


    }
}
