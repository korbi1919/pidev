﻿using PIdevDomain.Entities;
using PIdevService;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace PImvc.Controllers
{
    public class ClientController : Controller
    {

        private readonly IClientService clientService;
       
        public ClientController(IClientService clientService)
        {

            this.clientService = clientService;
            
        }
        //
        // GET: /Client/
        public ActionResult Index()
        {
           
            var farmers = clientService.GetFarmers();
          
            return View(farmers);
        }

        [HttpGet]
        public ActionResult Edit(int id)
        {

            var c = clientService.GetClient(id);
            return View(c);
        }






        

        [HttpGet]
        public ActionResult Create()
        {
            var Farmer = new Farmer();
            return View(Farmer);
        }
        //
        // POST: /Client/Create
        [HttpPost]
        public ActionResult Create(Farmer f)
        {
            if (!ModelState.IsValid)
            {
                return View(f);
            }
            clientService.CreateFarmer(f);
            clientService.SaveClient();
            return RedirectToAction("Index");
        }

        [HttpGet]
        public ActionResult Delete(int id)
        {

            //var c = clientService.GetClient(id);
            //return View(c);
            clientService.DeleteClient(id);
            clientService.SaveClient();
            //var farmers = clientService.GetFarmers();
            return RedirectToAction("Index");
        }

        [HttpPost]
        public ActionResult Edit(Farmer farmer)
        {


            if (ModelState.IsValid)
            {
                clientService.UpdateFarmerDetached(farmer);
                clientService.SaveClient();
                return RedirectToAction("Index");
            }
            return View(farmer);

        }

        //[HttpPost]
        //public ActionResult Delete(Farmer f)
        //{

        //    clientService.DeleteClient(f);
        //    clientService.SaveClient();
        //    //  var farmers = clientService.GetClient(id);
        //    //  return PartialView("CategoryList", farmers);
        //    // var farmers = clientService.GetFarmers();

        //    return RedirectToAction("Index");
        //}


       
    }
}
