﻿using PIdevData.Infrastructure;
using PIdevDomain.Entities;
using PIdevService;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PIdevService
{
    public interface IClientService
    {
        
        IEnumerable<Farmer> GetFarmer(bool isapproved);
     Client GetClient(int id);
     Client GetVeterinary(int id);


     //  Farmer GetfermierByid(int id);
       Client GetClientp(String pwd);

      // Client GetClientp(int Id);
       //Client verify(String log, String pwd);
        IEnumerable<Farmer> GetFarmer(String region);
        IEnumerable<Veterinary> GetVetReg(String region);

        IEnumerable<Veterinary> GetVeterinaries();
        IEnumerable<Farmer> GetFarmer();
        void CreateFarmer(Farmer f);
        void CreateAdmin(Admin a);
        void CreateVeterinary(Veterinary v);

        void CreateClient(Client c);
        void DeleteClient(int id);
        void DeleteClient(Client f);
        void DeleteClientV(int id);

        void SaveClient();
      void DeleteFarmer(Farmer f);
      void DeleteVeterinary(Veterinary f);

      IEnumerable<Farmer> GetFarmers();

        void UpdateFarmerDetached(Farmer e);
        void UpdateAdminDetached(Admin e);
        void UpdateVeterinaryDetached(Veterinary e);
    }
}


public class ClientService : IClientService
{
    static DatabaseFactory dbFactory = new DatabaseFactory();
    IUnitOfWork utOfWork = new UnitOfWork(dbFactory);
    public ClientService()
    { }

    #region IClientService Members
    IEnumerable<Farmer> IClientService.GetFarmer(String region)
    {

        IEnumerable<Farmer> farmersregi = utOfWork.FarmerRepository.GetMany(f => f.Region == region);

        return farmersregi;
    }



    IEnumerable<Veterinary> IClientService.GetVetReg(String region)
    {

        IEnumerable<Veterinary> vet = utOfWork.VeterinaryRepository.GetMany(f => f.Region == region);

        return vet;
    }


    Client IClientService.GetClient(int id)
    {
        var c = utOfWork.ClientRepository.GetById(id)  ;
        return c;
    }

    Client IClientService.GetVeterinary(int id)
    {
        var c = utOfWork.ClientRepository.GetById(id);
        return c;
    }

    //Farmer IClientService.GetfermierByid(int id)
    //{
    //    var c = utOfWork.FarmerRepository.GetById(id) ;
    //    return c;
    //}

    IEnumerable<Farmer> IClientService.GetFarmer(bool isapproved)
    {

        IEnumerable<Farmer> farmersNotInscri = utOfWork.FarmerRepository.GetMany(f => f.isconfirmed == isapproved);

        return farmersNotInscri;
    }



    IEnumerable<Farmer> IClientService.GetFarmer()
    {
        var fermier = utOfWork.ClientRepository.GetAll().OfType<Farmer>();
        return fermier;
    }

    Client IClientService.GetClientp(String pwd)
    {
        var c = utOfWork.ClientRepository.GetById(pwd);
        return c;
    }


    //Client IClientService.GetClient(String login)
    //{
    //    var c = utOfWork.ClientRepository.GetById(login);
    //    return c;
    //}





    void IClientService.CreateFarmer(Farmer c)
    {
        utOfWork.ClientRepository.Add(c);
    }


     void IClientService.CreateVeterinary(Veterinary c)
    {
        utOfWork.ClientRepository.Add(c);
    }
    void IClientService.CreateClient(Client c)
    {
        utOfWork.ClientRepository.Add(c);
    }
    void IClientService.CreateAdmin(Admin b)
    {
        utOfWork.ClientRepository.Add(b);
    }
    void IClientService.DeleteFarmer(Farmer f)
    {

        utOfWork.FarmerRepository.Delete(f);
    }


    void IClientService.DeleteVeterinary(Veterinary f)
    {

        utOfWork.VeterinaryRepository.Delete(f);
    }

    public void DeleteClient(int id)
    {
        var c = utOfWork.ClientRepository.GetById(id) as Farmer;
        utOfWork.ClientRepository.Delete(c);


    }

    public void DeleteClientV(int id)
    {
        var c = utOfWork.ClientRepository.GetById(id) as Veterinary;
        utOfWork.ClientRepository.Delete(c);


    }

    void IClientService.DeleteClient(Client f)
    {

        utOfWork.ClientRepository.Delete(f);
    }


    
    void IClientService.SaveClient()
    {
        utOfWork.Commit();
    }

    IEnumerable<Farmer> IClientService.GetFarmers()
    {
        var Farmers = utOfWork.ClientRepository.GetAll().OfType<Farmer>();
        return Farmers;
    }


    IEnumerable<Veterinary> IClientService.GetVeterinaries()
    {
        var veterinaries = utOfWork.ClientRepository.GetAll().OfType<Veterinary>();
        return veterinaries;
    }





    public void UpdateFarmerDetached(Farmer e)
    {
        utOfWork.ClientRepository.UpdateFarmerDetached(e);
    }






    public void UpdateVeterinaryDetached(Veterinary e)
    {
        utOfWork.ClientRepository.UpdateVeterinaryDetached(e);
    }

    public void UpdateAdminDetached(Admin e)
    {
        utOfWork.ClientRepository.UpdateAdminDetached(e);
    }
    #endregion
}


