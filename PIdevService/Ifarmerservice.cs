﻿using PIdevData.Infrastructure;
using PIdevDomain.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PIdevService
{
   public interface Ifarmerservice
    {
        //IEnumerable<Farmer> GetFarmer(String region);
        //IEnumerable<Farmer> GetFarmer();
        void CreateFarmer(Farmer f);
        void SaveFarmer();
        //void DeleteFarmer(Farmer f);

    }


    public class FarmerService:Ifarmerservice
    {
         static DatabaseFactory dbFactory=new DatabaseFactory(); 
    IUnitOfWork utOfWork = new UnitOfWork(dbFactory); 
    public FarmerService() 

    { 
    }

    void Ifarmerservice.CreateFarmer(Farmer c)
    {
        utOfWork.FarmerRepository.Add(c);
    }

    void Ifarmerservice.SaveFarmer()
    {
        utOfWork.Commit();
    } 

    }
}

