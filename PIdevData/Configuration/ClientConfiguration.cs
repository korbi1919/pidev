﻿using PIdevDomain.Entities;
using System;
using System.Collections.Generic;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PIdevData.Configuration
{
    public class ClientConfiguration : EntityTypeConfiguration<Client>
    {
        public ClientConfiguration()
        {
            ToTable("Clients");
            Property(u => u.ClientId).IsRequired();
            Property(u => u.Login).IsRequired();
            Property(u => u.Pwd).IsRequired();
            Property(u => u.Region).IsRequired();
            Property(u => u.Picture).IsOptional();
            Property(u => u.FirstName).HasMaxLength(100);
            Property(u => u.LastName).HasMaxLength(100);
            Property(u => u.Mail).HasMaxLength(100);
            Property(u => u.Address).HasMaxLength(500);

            Map<Farmer>(c => 
            {
                c.Requires("IsFarmer").HasValue(1);
               
            })
            
            .Map<Admin>(c =>
            {
                c.Requires("IsFarmer").HasValue(0);
            })

             .Map<Veterinary>(c =>
            {
                c.Requires("IsFarmer").HasValue(2);
            });


        }
    }
}
