﻿using PIdevData.Configuration;
using PIdevDomain.Entities;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PIdevData
{
    public class SheepManagementContext : DbContext
    {
        public SheepManagementContext()
            : base("SheepManagement")
        {
            
        } 
       
        public DbSet<Client> Clients { get; set; }
      
        // public DbSet<Sheep> Sheeps { get; set; }
       
               
        public virtual void Commit()
        {
            base.SaveChanges();
        }
        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Configurations.Add(new ClientConfiguration());

            // modelBuilder.Conventions.Remove<IncludeMetadataConvention>(); 
          

        }

        public System.Data.Entity.DbSet<PIdevDomain.Entities.Farmer> Farmers { get; set; }

        public System.Data.Entity.DbSet<PIdevDomain.Entities.Veterinary> Veterinaries { get; set; }
    }







}
