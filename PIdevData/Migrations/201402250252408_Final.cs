namespace PIdevData.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Final : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Clients",
                c => new
                    {
                        ClientId = c.Int(nullable: false, identity: true),
                        Login = c.String(nullable: false),
                        Pwd = c.String(nullable: false),
                        FirstName = c.String(maxLength: 100),
                        LastName = c.String(maxLength: 100),
                        Picture = c.String(),
                        Mail = c.String(maxLength: 100),
                        Region = c.String(nullable: false),
                        Address = c.String(maxLength: 500),
                        isconfirmed = c.Boolean(),
                        isinscri = c.Boolean(),
                        IsFarmer = c.Int(),
                    })
                .PrimaryKey(t => t.ClientId);
            
        }
        
        public override void Down()
        {
            DropTable("dbo.Clients");
        }
    }
}
