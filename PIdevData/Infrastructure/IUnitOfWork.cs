﻿
using PIdevData.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PIdevData.Infrastructure


{
    public interface IUnitOfWork { 
        void Commit();
                   
        IClientRepository ClientRepository { get; }
        IFarmerRepository FarmerRepository { get; }
        IVeterinaryRepository VeterinaryRepository { get; }
        
        
         }
}
