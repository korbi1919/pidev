﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
namespace PIdevData.Infrastructure


{
  public interface IDatabaseFactory: IDisposable
    {
      SheepManagementContext Get();


    }
}
