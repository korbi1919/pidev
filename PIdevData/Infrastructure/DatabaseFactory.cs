﻿using PIdevData;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PIdevData.Infrastructure

{
    //gerer le context
    public class DatabaseFactory : Disposable, IDatabaseFactory
    {

        private SheepManagementContext dataContext = null;
        public SheepManagementContext Get()
        {
            return dataContext ?? (dataContext = new SheepManagementContext()); 
        } 
        protected override void DisposeCore()
        { 
            if (dataContext != null) dataContext.Dispose();
        }

    }
}
