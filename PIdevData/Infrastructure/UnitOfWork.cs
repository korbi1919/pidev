﻿
using PIdevData.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PIdevData.Infrastructure


{
    public class UnitOfWork : IUnitOfWork 
    {
        private SheepManagementContext dataContext;
        DatabaseFactory dbFactory;
        public UnitOfWork(DatabaseFactory dbFactory)
        {
            this.dbFactory = dbFactory;
        }
       
        private IClientRepository UserRepository;
        IClientRepository IUnitOfWork.ClientRepository
        {
            get
            {
                return UserRepository ?? (UserRepository = new ClientRepository(dbFactory));
            }
        }


        private IFarmerRepository farmerRepository;
        IFarmerRepository IUnitOfWork.FarmerRepository
        {
            get
            {
                return farmerRepository ?? (farmerRepository = new FarmerRepository(dbFactory));
            }
        }


        private IVeterinaryRepository veterinaryRepository;
        IVeterinaryRepository IUnitOfWork.VeterinaryRepository
        {
            get
            {
                return veterinaryRepository ?? (veterinaryRepository = new VeterinaryRepository(dbFactory));
            }
        }

            

        protected SheepManagementContext DataContext
        {
            get
            {
                return dataContext ?? (dataContext = dbFactory.Get());
            }
        }
        public void Commit()
        {
            DataContext.Commit();
        }
    }
}
