﻿using PIdevData.Infrastructure;
using PIdevDomain.Entities;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PIdevData.Repositories
{
    public class ClientRepository : RepositoryBase<Client>, IClientRepository
    {
        public ClientRepository(DatabaseFactory dbFactory)
            : base(dbFactory)
        {


        }

        public void UpdateFarmerDetached(Farmer e)
        {
            Farmer existing = this.DataContext.Clients.OfType<Farmer>().FirstOrDefault(x => x.ClientId == e.ClientId);
            ((IObjectContextAdapter)DataContext).ObjectContext.Detach(existing);
            this.DataContext.Entry(e).State = EntityState.Modified;
        }


        public void UpdateAdminDetached(Admin e)
        {
            Admin existing = this.DataContext.Clients.OfType<Admin>().FirstOrDefault(x => x.ClientId == e.ClientId);
            ((IObjectContextAdapter)DataContext).ObjectContext.Detach(existing);
            this.DataContext.Entry(e).State = EntityState.Modified;
        }

        public void UpdateVeterinaryDetached(Veterinary e)
        {
            Veterinary existing = this.DataContext.Clients.OfType<Veterinary>().FirstOrDefault(x => x.ClientId== e.ClientId);
            ((IObjectContextAdapter)DataContext).ObjectContext.Detach(existing);
            this.DataContext.Entry(e).State = EntityState.Modified;
        }


    }
    public interface IClientRepository : IRepository<Client>
    {
       void UpdateFarmerDetached(Farmer e);
        void UpdateAdminDetached(Admin e);
        void UpdateVeterinaryDetached(Veterinary e);

    }
}
