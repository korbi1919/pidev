﻿using PIdevData.Infrastructure;
using PIdevDomain.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PIdevData.Repositories
{
    public class VeterinaryRepository : RepositoryBase<Veterinary>, IVeterinaryRepository
    {

        public VeterinaryRepository(DatabaseFactory dbFactory)
            : base(dbFactory)
        {


        }

    }


    public interface IVeterinaryRepository : IRepository<Veterinary>
    {
        //void UpdateFarmerDetached(Farmer e); 
        // void UpdateAdminDetached(Admin e); }

    }
}
