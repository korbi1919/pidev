﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PIdevDomain.Entities
{
   public class Client 
    {
       [Key]
       public int ClientId { get; set; }
       public String Login { get; set; }

       public String Pwd { get; set; }
        public String FirstName { get; set; }
        public String LastName { get; set; }
        public string Picture { get; set; }

        public String Mail { get; set; }

        public String Region { get; set; }
        public String Address { get; set; }

       

    }
}
